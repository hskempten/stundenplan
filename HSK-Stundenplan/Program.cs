﻿using System;
using System.Xml;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace HSK.Stundenplan
{
    class Program
    {
        public static void Main()
        {
            InvokeService("st...", "...", "schedule");
        }

        public static HttpWebRequest CreateSOAPWebRequest()
        {
            HttpWebRequest Req =
                (HttpWebRequest) WebRequest.Create(
                    @"https://online-verwaltung.hs-kempten.de/qisserver/services/dbinterface");
            Req.Headers.Add(@"SOAPAction:https://online-verwaltung.fh-kempten.de/qisserver/services/dbinterface");
            Req.ContentType = "application/soap+xml;charset=UTF-8";
            Req.Accept = "text/xml";
            Req.Method = "POST";
            return Req;
        }

        public static void InvokeService(string username, string password, string paramString)
        {
            HttpWebRequest request = CreateSOAPWebRequest();

            XmlDocument requestBody = new XmlDocument();
            requestBody.LoadXml(
                "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dbin=\"http://dbinterface.xml.his.de\"><soapenv:Header/><soapenv:Body><dbin:getDataXML soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><xmlParams xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">&lt;SOAPDataService&gt;&lt;general&gt;&lt;object&gt;" +
                paramString + "&lt;/object&gt;&lt;/general&gt;&lt;user-auth&gt;&lt;username&gt;" + username +
                "&lt;/username&gt;&lt;password&gt;" + password +
                "&lt;/password&gt;&lt;/user-auth&gt;&lt;/SOAPDataService&gt;</xmlParams></dbin:getDataXML></soapenv:Body></soapenv:Envelope>");

            using (Stream stream = request.GetRequestStream())
                requestBody.Save(stream);

            using (WebResponse webResponse = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    var result = rd.ReadToEnd();
                    result = result.Replace("&lt;", "<").Replace("&gt;", ">");

                    Console.WriteLine(result);
                    result = "<scheduleChangeAll" + result.Split("scheduleChangeAll")[1] + "scheduleChangeAll>";
                    
                    Console.WriteLine(result);

                    XmlSerializer serializer = new XmlSerializer(typeof(ScheduleChangeAll));
                    using (StringReader reader = new StringReader(result))
                    {
                        var changeAll = (ScheduleChangeAll) serializer.Deserialize(reader);
                        foreach (var lecture in changeAll.Lecture)
                        {
                            Console.WriteLine(lecture.Veranstaltung + " - " + lecture.Bemerkung);
                        }
                    }

                    Console.ReadLine();
                }
            }
        }
    }
}