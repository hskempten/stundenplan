﻿/* 
    Licensed under the Apache License, Version 2.0
    
    http://www.apache.org/licenses/LICENSE-2.0
    */

using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace HSK.Stundenplan
{
    [XmlRoot(ElementName = "lecturer")]
    public class Lecturer
    {
        [XmlElement(ElementName = "personal.nachname")]
        public string nachname { get; set; }
    }

    [XmlRoot(ElementName = "lecture")]
    public class Lecture
    {
        [XmlElement(ElementName = "datum")] public string Datum { get; set; }
        [XmlElement(ElementName = "beginn")] public string Beginn { get; set; }
        [XmlElement(ElementName = "ende")] public string Ende { get; set; }

        [XmlElement(ElementName = "veranstnr")]
        public string Veranstnr { get; set; }

        [XmlElement(ElementName = "veranstaltung")]
        public string Veranstaltung { get; set; }

        [XmlElement(ElementName = "vtid")] public string Vtid { get; set; }

        [XmlElement(ElementName = "einrichtung")]
        public string Einrichtung { get; set; }

        [XmlElement(ElementName = "gebaeude")] public string Gebaeude { get; set; }
        [XmlElement(ElementName = "raum")] public string Raum { get; set; }

        [XmlElement(ElementName = "parallelgruppe")]
        public string Parallelgruppe { get; set; }

        [XmlElement(ElementName = "bemerkung")]
        public string Bemerkung { get; set; }

        [XmlElement(ElementName = "typ")] public string Typ { get; set; }
        [XmlElement(ElementName = "lecturer")] public List<Lecturer> Lecturer { get; set; }
    }

    [XmlRoot(ElementName = "scheduleChangeAll")]
    public class ScheduleChangeAll
    {
        [XmlElement(ElementName = "lecture")] public List<Lecture> Lecture { get; set; }
    }

    [XmlRoot(ElementName = "getDataXMLReturn")]
    public class GetDataXMLReturn
    {
        [XmlElement(ElementName = "scheduleChangeAll")]
        public ScheduleChangeAll ScheduleChangeAll { get; set; }

        // [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        // public string Type { get; set; }
        //
        // [XmlAttribute(AttributeName = "soapenc", Namespace = "http://www.w3.org/2000/xmlns/")]
        // public string Soapenc { get; set; }
    }

    [XmlRoot(ElementName = "getDataXMLResponse", Namespace = "http://dbinterface.xml.his.de")]
    public class GetDataXMLResponse
    {
        [XmlElement(ElementName = "getDataXMLReturn")]
        public GetDataXMLReturn GetDataXMLReturn { get; set; }

        // [XmlAttribute(AttributeName = "encodingStyle", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        // public string EncodingStyle { get; set; }
        //
        // [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        // public string Ns1 { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body
    {
        [XmlElement(ElementName = "getDataXMLResponse", Namespace = "http://dbinterface.xml.his.de")]
        public GetDataXMLResponse GetDataXMLResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }

        // [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        // public string Soapenv { get; set; }
        //
        // [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        // public string Xsd { get; set; }
        //
        // [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        // public string Xsi { get; set; }
    }
}